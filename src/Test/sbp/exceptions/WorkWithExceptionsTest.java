package sbp.exceptions;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class WorkWithExceptionsTest
    {
    @Test
    public void throwUncheckedException()
        {
        WorkWithExceptions workWithExceptions = new WorkWithExceptions();
        Assertions.assertThrows(RuntimeException.class, () -> workWithExceptions.throwUncheckedException());
        }

    @Test
    public void throwCheckedException()
        {
        WorkWithExceptions workWithExceptions = new WorkWithExceptions();
        Assertions.assertThrows(Exception.class, () -> workWithExceptions.throwCheckedException());
        }

    @Test
    public void hardExceptionProcessing()
        {
        WorkWithExceptions workWithExceptions = new WorkWithExceptions();
        Assertions.assertThrows(RuntimeException.class, () -> workWithExceptions.hardExceptionProcessing());
        }
    }