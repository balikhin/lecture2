package sbp.io;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertTrue;


public class MyIOExampleTest
    {

    @Test
    public void workWithFileFalseTest() throws IOException
        {
        MyIOExample.MyIOExampleBody myIOExampleBody = new MyIOExample.MyIOExampleBody();
        MyIOExample myIOExample = new MyIOExample();

        File fileName1 = new File("fileName");

        Assertions.assertFalse(fileName1.exists());
        Assertions.assertFalse(myIOExampleBody.workWithFile("fileName"));

        System.out.println("Metod return FALSE and files NOT exists");
        }

    @Test
    public void workWithFileTrueTest() throws IOException
        {
        MyIOExample.MyIOExampleBody myIOExampleBody = new MyIOExample.MyIOExampleBody();
        MyIOExample myIOExample = new MyIOExample();
        File fileName1 = new File("fileName");

        assertTrue(myIOExampleBody.workWithFile("file"));
        assertTrue(fileName1.exists());

        System.out.println("Metod return TRUE and files EXISTSs");
        }

    @Test
    public void copyFileTest() throws IOException
        {
        MyIOExample.MyIOExampleBody myIOExampleBody = new MyIOExample.MyIOExampleBody();
        MyIOExample myIOExample = new MyIOExample();

        File fileName1 = new File("sourceFileName");
        File fileName2 = new File("destinationFileName");

        assertTrue(myIOExampleBody.copyFile("sourceFileName", "destinationFileName"));
        assertTrue(fileName1.exists());
        assertTrue(fileName2.exists());

        System.out.println("Metod return TRUE and files EXISTSs");
        }

    @Test
    public void copyBufferedFileTest() throws IOException
        {
        MyIOExample.MyIOExampleBody myIOExampleBody = new MyIOExample.MyIOExampleBody();
        MyIOExample myIOExample = new MyIOExample();

        File fileName1 = new File("sourceFileName1");
        File fileName2 = new File("destinationFileName1");

        assertTrue(myIOExampleBody.copyBufferedFile("sourceFileName", "destinationFileName"));
        assertTrue(fileName1.exists());
        assertTrue(fileName2.exists());

        System.out.println("Metod return TRUE and files EXISTSs");
        }

    @Test
    public void copyFileWithReaderAndWriterTest() throws IOException
        {
        MyIOExample.MyIOExampleBody myIOExampleBody = new MyIOExample.MyIOExampleBody();
        MyIOExample myIOExample = new MyIOExample();

        File fileName = new File("sourceFileName2");
        File fileName2 = new File("destinationFileName2");

        assertTrue(myIOExampleBody.copyFileWithReaderAndWriter("sourceFileName", "destinationFileName"));
        assertTrue(fileName.exists());
        assertTrue(fileName2.exists());

        System.out.println("Metod return TRUE and files EXISTSs");
        }
    }