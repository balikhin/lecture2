package sbp.exceptions;

public class WorkWithExceptions
    {
    /**
     * В данном методе необходимо вызвать методы throwCheckedException и throwUncheckedException.
     * Все исключения должны быть обработаны
     * Необходимо вывести описание exception и stacktrace в консоль
     * Впойманное исключение необходимо упаковать в кастомное исключение и пробросить выше
     * Перед завершением работы метода обязательно необходимо вывести в консоль сообщение "Finish"
     */
    public static void main(String[] args) throws CustomException
        {
        try
            {
            throwUncheckedException();
            throwCheckedException();
            } catch (RuntimeException e)
            {
            System.out.println("Error: " + e.getMessage());
            e.printStackTrace();
            throw new CustomException("to WorkWithExceptions class ", e);
            } catch (Exception e)
            {
            System.out.println("Error: " + e.getMessage());
            e.printStackTrace();
            }

        }

    public static class CustomException  extends Exception
        {
        public CustomException(String message, Throwable cause)
            {
            super(message + " my custom exception ", cause);
            System.out.println("FINISH");
            }
        }

    public static void throwCheckedException() throws Exception
        {
        throw new Exception("my Exception");
        }

    public static void throwUncheckedException()
        {
        int i = 5 / 0;
        throw new RuntimeException("my UncheckedException");
        }



    /**
     * (* - необязательно) Доп. задание.
     * Выписать в javadoc (здесь) - все варианты оптимизации и устранения недочётов метода
     * использовать мультикетч с использование │ т.е. catch (IllegalArgumentException e)
     *
     * Я бы изменил код так, добавил бы в тело метода main:
     *  try {
     *             hardExceptionProcessing();
     *         } catch (Exception e) {
     *             System.out.println("Error: " + e.getMessage());
     *             //e.printStackTrace();
     *         }
     * а в этом методе все удалил:
     * public static void hardExceptionProcessing() throws IllegalStateException, Exception, RuntimeException
     *     { /////// тело метода }
     *
     *
     * @throws IllegalStateException
     * @throws Exception
     * @throws RuntimeException
     */

    public static void hardExceptionProcessing() throws IllegalStateException, Exception, RuntimeException
        {
        System.out.println("Start");
        try
            {
              System.out.println("Step 1");
              throw new IllegalArgumentException();
            }
        catch (IllegalArgumentException e)
            {
              System.out.println("Catch IllegalArgumentException");
              throw new RuntimeException("Step 2");
            }
        catch (RuntimeException e)
            {
              System.out.println("Catch RuntimeException");
              throw new RuntimeException("Step 3");
            }
        finally
            {
            System.out.println("Step finally");
            throw new RuntimeException("From finally");
            }
        }

  /*private void throwCheckedException() throws Exception
    {
        throw new Exception("Checked exception");
    }

    private void throwUncheckedException()
    {
        throw new RuntimeException("Unchecked exception");
    }*/
    }