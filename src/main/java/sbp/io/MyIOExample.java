package sbp.io;

import java.io.*;
import java.util.Date;

public class MyIOExample
    {

    public static void main(String[] args)
        {

        MyIOExampleBody ioExample = new MyIOExampleBody ();
        try {
        System.out.println("Return from WorkWithFile: " + ioExample.workWithFile("file"));
        System.out.println("Return from copyFile: " + ioExample.copyFile("file", "secondFile"));
        System.out.println("Return from copyBufferedFile: " + ioExample.copyBufferedFile("file", "secondFile"));
        System.out.println("Return from copyFileWithReaderAndWriter: " + ioExample.copyFileWithReaderAndWriter("file", "secondFile"));


        } catch (IOException e) {
        System.out.println("Error: " + e.getMessage());
        }

        }

    public static class MyIOExampleBody {
    /**
     * Создать объект класса {@link java.io.File}, проверить существование и чем является (фалй или директория).
     * Если сущность существует, то вывести в консоль информацию:
     * - абсолютный путь
     * - родительский путь
     * Если сущность является файлом, то вывести в консоль:
     * - размер
     * - время последнего изменения
     * Необходимо использовать класс {@link java.io.File}
     *
     * @param fileName - имя файла
     * @return - true, если файл успешно создан
     */
    public boolean workWithFile(String fileName) throws IOException {

    File fileName1 = new File("fileName");

    System.out.println("filename.exist: " + fileName1.exists());
    if (fileName1.exists() && fileName1.isFile()) {
    System.out.println("Absolut path: " + fileName1.getAbsolutePath());
    System.out.println("Parents path: " + fileName1.getParent());
    System.out.println("Time of last change: " + new Date(fileName1.lastModified()));

    if (fileName1.exists()) {
    System.out.println("file size: " + fileName1.length() + " bytes");

    } else System.out.println("Файла нет!");

    } else {
    System.out.println("filename.isFile: " + fileName1.isFile());
    System.out.println("filename.isDirectory: " + fileName1.isDirectory());
    }


    if (fileName1.exists()) {
    return true;
    } else {
    FileWriter writer = new FileWriter("fileName");
    writer.write("Hello world!!");
    writer.close();
    return false;
    }
    }
    /**
     * Метод должен создавать копию файла
     * Необходимо использовать IO классы {@link java.io.FileInputStream} и {@link java.io.FileOutputStream}
     *
     * @param sourceFileName      - имя исходного файла
     * @param destinationFileName - имя копии файла
     * @return - true, если файл успешно скопирован
     */


    public boolean copyFile(String sourceFileName, String destinationFileName) throws IOException
        {
        File fileName1 = new File("sourceFileName");
        File fileName2 = new File("destinationFileName");

        FileWriter writer = new FileWriter("sourceFileName");
        writer.write("Hello world!! from copyFile");
        writer.close();

        InputStream is = null;
        OutputStream os = null;

        if (fileName2.exists()) {
        return true;
        } else {
        try {
        is = new FileInputStream(fileName1);
        os = new FileOutputStream(fileName2);
        byte[] buffer = new byte[1024];
        int length;
        while ((length = is.read(buffer)) > 0) {
        os.write(buffer, 0, length);
        }
        } finally
            { try
                { if (is != null)
                    {
                    is.close();
                    }
                if (os != null)
                    {
                    os.close();;
                    }
                } catch (Exception e2)
                {
                e2.printStackTrace();
                }
            }
        }
        return true;
        }


    /**
     * Метод должен создавать копию файла
     * Необходимо использовать IO классы {@link java.io.BufferedInputStream} и {@link java.io.BufferedOutputStream}
     *
     * @param sourceFileName      - имя исходного файла
     * @param destinationFileName - имя копии файла
     * @return - true, если файл успешно скопирован
     */
    public boolean copyBufferedFile(String sourceFileName, String destinationFileName) throws IOException
        {
        File fileName1 = new File("sourceFileName1");
        File fileName2 = new File("destinationFileName1");

        FileWriter writer = new FileWriter("sourceFileName1");
        writer.write("Hello world!! from copyBufferedFile");
        writer.close();

        if (fileName2.exists()) {
        return true; } else
            {
            try {
            byte[] buffer = new byte[1000];
            FileInputStream fis = new FileInputStream("sourceFileName1");
            BufferedInputStream bis = new BufferedInputStream(fis);

            FileOutputStream fos = new FileOutputStream("destinationFileName1");
            BufferedOutputStream bos = new BufferedOutputStream(fos);

            int numBytes;
            while ((numBytes = bis.read(buffer)) != -1) {
            bos.write(buffer, 0, numBytes);
            }

            bis.close();
            bos.close();
            } finally
                {
                return true;
                }


            }
        }

    /**
     * Метод должен создавать копию файла
     * Необходимо использовать IO классы {@link java.io.FileReader} и {@link java.io.FileWriter}
     *
     * @param sourceFileName      - имя исходного файла
     * @param destinationFileName - имя копии файла
     * @return - true, если файл успешно скопирован
     */
    public boolean copyFileWithReaderAndWriter(String sourceFileName, String destinationFileName) throws IOException
        {
        FileWriter writer = new FileWriter("sourceFileName2");
        writer.write("Hello world!! from copyFileWithReaderAndWriter");
        writer.close();

        File fileName = new File("sourceFileName2");
        FileReader fr = new FileReader(fileName);

        File fileName2 = new File("destinationFileName2");
        FileWriter fw = new FileWriter(fileName2);

        int a;
        if (fileName2.exists()) {
        return true;
        } else
            { try
                {
                while ((a = fr.read()) != -1)
                    {
                    fw.write(a);
                    }
                } finally
                {
                fw.close();
                fr.close();
                }
            }
        return true;
        }
    }
    }