package sbp.user;

import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;


public class User
    {

    public static void main(String[] args) throws IOException
        {
        Scanner in = new Scanner(System.in);
        Person person1 = new Person();
        Login login = new Login();
        Password password = new Password();

        System.out.print("Введите имя пользователя: ");
        String name = in.nextLine();

        System.out.print("Введите возраст пользователя: ");
        int num = in.nextInt();

        person1.setName(name);
        person1.setAge(num);

        System.out.println("Выводим данные пользователя: " + person1.getName() + " лет " + person1.getAge());
        person1.speak();
        System.out.println("и мне до пенсии оталось: " + person1.calculateYeahrToRetriment() + " лет.");

        String s = login.data();
        System.out.println( s );
        String p = password.data();
        System.out.println( p );
        in.close();
        }
    }

class Person
    {
    private String name;
    private int age;

    public Person(){}
    public Person(String string)
        {
        System.out.println("Создан конструктор буз возраста!");
        }

    public Person(String name, int age)
        {
        System.out.println("Создан второй конструктор");
        this.name = name;
        this.age = age;
        }

    public void setName(String name)
        {
        if (name.isEmpty())
            {
            System.out.println("Имя и возраст введены корректно");
            }  else
            this.name = name;
        }
    public String getName()
        {
        return name;
        }
    public void setAge(int age)
        {
        if (age > 0)
            {
            this.age = age;
            }
        else System.out.println("Возраст должен быть положительным!");


        }
    public int getAge()
        {
        return age;
        }

    int calculateYeahrToRetriment()
        {
        return 65 - age;
        }

    boolean speak()
        {
        for(int i = 0; i < 1; i++)
            System.out.print("Меня зовут " +this.name + " мне "+ this.age + " лет ");
        return true;
        }

    String data() throws IOException
        {
        return null;
        }
    }

class Login extends Person
    {
    @Override
    String data() throws IOException
        {
        Scanner in = new Scanner(System.in);
        String str = "Привет из класса Login, отработали хорошо!";
        System.out.print("Введите Login пользователя: ");
        String log = in.nextLine();
        FileWriter writer = new FileWriter("login");
        writer.write(log);
        writer.close();
        return str;
        }
    }

class Password extends Person
    {
    @Override
    String data() throws IOException
        {
        Scanner in = new Scanner(System.in);
        String psw = "Привет из класса Password, отработали не плохо!";
        System.out.print("Введите password пользователя: ");

        String pas = in.nextLine();

        FileWriter writer = new FileWriter("password");
        writer.write(pas);
        writer.close();

        return psw;
        }
    }